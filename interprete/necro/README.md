#Running
Si un des dossiers `spec` ou `necro` est vide lancez les commandes

	$ git submodule init
	$ git submodule update

Si `necro` n'a pas été compilé faites :

	$ cd necro
	$ make
	$ cd ..

Puis

	$ make

Et enfin

	$ ./test.native
