% vim: set spelllang=en :

\todo{Autoref added some denomination that are not great (Equation, ...)
  we should take care of this -- Jean Jouve}

\subsection{Specification Paradigm}

\todo{Find something else that the word "ways" -- Jean Jouve}
\todo{If not enough space in paper, reduce the discussion on parallelism and
  while -- Jean Jouve}
There exists multiple different way to specify the semantic of languages. The
semantics written with the two most prominent ways in the domain are
\emph{big-step} semantics \citep{kahnNaturalSemantic} and \emph{small-step}
semantics \citep{plotkinOperationalSemantic}. Those two type of semantics
consist in inference rules describing how the program is derived in a value for
a given context.

The principle of \emph{big-step} semantics is to consider a program as a
relation between its inputs and output: the meaning of an expression is given
by its final value. The usual natural semantics of the \tif construct is given
by the two inference rules \autoref{eq:bigStepIfTrue} and
\autoref{eq:bigStepIfFalse}. Both represent the execution of an \tif
statement, the first one where the condition is \ttrue, the other one
where the condition is \tfalse.
\begin{gather}
  \label{eq:bigStepIfTrue}
  \begin{array}{c}
    \sigma \vdash e \Rightarrow \ttrue
    \quad \sigma \vdash s_1 \Rightarrow \sigma' \\
  \hline
    \sigma \vdash \tif e \tthen s_1 \telse s_2
      \Rightarrow \sigma'
  \end{array} \\
  \label{eq:bigStepIfFalse}
  \begin{array}{c}
    \sigma \vdash e \Rightarrow \tfalse
    \quad \sigma \vdash s_2 \Rightarrow \sigma' \\
  \hline
    \sigma \vdash \tif e \tthen s_1 \telse s_2
      \Rightarrow \sigma'
  \end{array}
\end{gather}
Where the $\sigma$ is the context in which the construct is evaluated and
$\sigma'$ is the resulting context. $s_1$ and $s_2$ are usually sequences of
statements of the language and $e$ is usually an expression.

This way of describing semantic is quite straightforward and easy to read.
However, many concepts can not be described in big-step, such as
non-terminating executions. It is indeed impossible to build the inference tree
of a program that does not terminate, since there is no execution with a final
state. For this reason it is also impossible to distinguish correct but
potentially non-terminating programs from incorrect ones. This idea of skipping
intermediate states of an execution of a program also forbids the analysis of
parallel executions.  Indeed, the final result of two concurrent programs will
be modelled as the pair of values resulting from the execution of each program:
the order of evaluation of the sub-expressions is not specified. The next
inference rule (\autoref{eq:parrallelism}) represents this parallel execution.
\begin{equation}
  \label{eq:parrallelism}
  \begin{array}{c}
    \sigma \vdash s_1 \Rightarrow \sigma_1
    \quad \rho \vdash s_2 \Rightarrow \sigma_2 \\
  \hline
    \rho \vdash (s_1||s_2) \Rightarrow \sigma'
  \end{array}
\end{equation}
In this particular example, the resulting environment $\sigma'$ can not be
trivially obtained without detailing smaller steps of computation. This is the
major drawback of \emph{Big-step} semantics.
 
\emph{Small-step} semantics fix this issue by modeling the behavior of the
language by describing minimal steps of computation. At each step, the
semantics considers a term to evaluate as well as the current state of the
memory. From this term and this state it computes a new term and a new state
based on a sequence of applicable inference rules. For instance, the
\emph{small-step} semantics for the if construct has two similar rules to the
\emph{big-step} ones (rules \autoref{eq:smallStepIfTrue} and
\autoref{eq:smallStepIfFalse}) but also adds a new rule, rule
\autoref{eq:smallStepIfCond}, to reduce the conditional term to a known value.

\begin{gather}
  \label{eq:smallStepIfTrue}
  \begin{array}{c}
    \hline
    \sigma \vdash \tif \ttrue \tthen s_1 \telse s_2
      \Rightarrow \sigma, s_1
  \end{array} \\
  \label{eq:smallStepIfFalse}
  \begin{array}{c}
    \hline
    \sigma \vdash \tif \tfalse \tthen s_1 \telse s_2 \Rightarrow \sigma, s_2
  \end{array} \\
  \label{eq:smallStepIfCond}
  \begin{array}{c}
    \sigma \vdash e \Rightarrow \sigma, e'\\
    \hline
    \sigma \vdash \tif e \tthen s_1 \telse s_2
      \Rightarrow \sigma, \tif e' \tthen s_1 \telse s_2
  \end{array}
\end{gather}

Indeed, contrary to \emph{big-step} semantics which would completely evaluate
the term $e$ to either $\ttrue$ or $\tfalse$, \emph{small-step} semantics will
detail all the evaluation of $e$, step by step. Since the derivation of
inference rules describes each step of the evaluation of the program instead of
evaluating the whole program at once, it is now possible to consider an
execution order, thus giving a meaning to interlacing in parallelism. Let us
take the example of a parallel execution (see rules
\autoref{eq:smallStepParallel1} and \autoref{eq:smallStepParallel2}).
\begin{gather}
  \label{eq:smallStepParallel1}
  \begin{array}{c}
    \sigma \vdash s_1 \Rightarrow \sigma',  s_1' \\
    \hline
    \sigma \vdash (s_1 || s_2) \Rightarrow \sigma', (s_1' || s_2)
  \end{array} \\
  \label{eq:smallStepParallel2}
  \begin{array}{c}
    \sigma \vdash s_2 \Rightarrow \sigma', s_2'  \\
    \hline
    \sigma \vdash (s_1 || s_2) \Rightarrow \sigma', (s_1 || s_2')
  \end{array}
\end{gather}

There is now the choice to either compute a minimal step from the first or the
second term, until both of them are finally computed.

This improved expressivity comes at a cost readability. In a \emph{small-step}
semantics, the $\twhile$ construct is expected to be evaluated by progressively
reducing the condition before evaluating the body.  However, the initial
condition term would be lost and could not be evaluated again in a new
environment after looping in the $\twhile$. For this reason, the $\twhile$
construct is transformed to an $\tif$ construct followed by the same while
construct (see rule \autoref{eq:smallStepWhile}).
\begin{equation}
  \label{eq:smallStepWhile}
  \begin{array}{c}
    \hline
    \sigma \vdash \twhile e \tdo s
    \Rightarrow \sigma, \tif e \tthen s ; \twhile e \tdo s \telse ()
    \end{array}
\end{equation}
This trick used to represent the $\twhile$ construct brings the creation of
virtual sub-terms. It is indeed necessary at some steps to compute a new term
that has no real meaning. This gives an unnatural evaluation of some terms,
that does not reflect the reality. 

\subsection{Meta-languages}

There exists different semantics --- operational semantic, abstract semantic,
static semantic, ...  --- and different way of expressing them --- big-step,
small-step --- that are all useful depending on the need. Therefore, there is
sometimes the need to rewrite the semantics of a language to a different form
or semantics and as a result rewrite everything that is based on the semantic.
This was the case for \emph{CompCert}, a Coq certified C compiler. Its
certification was originally written over a \emph{big-step} semantics but the
developers had to change to a \emph{small-step} semantics and thereby rewrite
the whole certification. Since the different semantics describe the same
things, some tools have been created to unify the different semantic and as a
result avoid the tedious rewriting of everything that is based on the semantic
when changing the semantics.

These tools are called meta-language, they are usually a syntax that can be
interpreted in different kinds of semantic such as an interpreter or a type
checker and different presentation of those semantics such as a \LaTeX file or
some proof assistant code.

The meta-language \emph{Ott} \citep{sewellOtt} is one of the first
meta-language specifically designed to export its own formal definitions to
other proofs assistants. \emph{Ott} was a good step forward in the description of
semantics, yet not capable of directly exporting properties nor proofs. This
forces large-scale projects which use different proof assistants to redefine
each needed property in different assistants, despite centralizing definitions.

The meta-language \emph{Lem} \citep{mulliganLem} was introduced to remove the
lack of property definition capability from \emph{Ott}, with hope to improve
portability. \emph{Lem} also adds logic constructors such as universal and
existential quantification for directly writing lemmas or properties in the
specification, which can then be exported to proof assistants, avoiding the
shortcoming \emph{Ott} encountered.

\emph{Lem} was used to specify the semantics of C11 and C++11 concurrency model
\citep{battyLemConcurrency} and to specify the semantics of the OCaml\_light
language \citep{owenLemOcamllight}. However, \emph{Lem} itself is lacking a
specification of its semantics. This makes the verification of compilation from
\emph{Lem} to proof assistant code impossible.  

\todo{Wait to see what Clement will have to say about K to add a comparison
  with K -- Jean Jouve}
Our contribution uses the meta-language \emph{skeletal semantics}. This
meta-language introduced by \citet{bodinSkeletalSemantic} consist in the
specification of relation between basic building blocks to form
\emph{skeletons}. Those relations and building blocks can then be interpreted
to form a semantic. This allows producing semantics with minimal work since the
interpretation only has to focus on the basic relations and not he whole
picture. Since the semantic of the meta-language is given by each
interpretation, it is always available to the user. This meta-language being
new, there is no language used in the industry that have been specified yet.
That is why we chose to specify a real world language using skeletal semantic
as a proof of concept for the meta-language.

\subsection{Web Assembly}

The programming language Web Assembly designers have made it a goal to specify
the language in a way that makes it easier to develop formal analysis and
specification for the language. In addition to this work on the specification a
formal semantic of the language in the form reduction rules has been given by
the working group \citep{rosbergWebAssembly} as well as an Ocaml interpreter
\citep{workingGroupWAInterpreter}. Thanks to the design and formal semantic of
Web Assembly a mechanisation of the formal semantic in Isabelle was written not
long after the language was introduced \citep{wattMechanising}. To the best of
our knowledge those are currently the only existing specification of Web
Assembly, there is no meta-language specification of the language. That is why
made us chose this language as the one to specify. Moreover, the
well-thought-out existing specification will make our job easier.
