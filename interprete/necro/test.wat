(module
    (func $main
          i32.const 5
          call $fact
    )
    (func $fact (param $n i32) (result i32)
        local.get $n
        i32.const 1
        i32.le_u 
        if (result i32)
            i32.const 1
        else
            local.get $n
            i32.const 1
            i32.sub
            call $fact
            local.get $n
            i32.mul
        end
    )
)
