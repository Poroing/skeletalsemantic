(module
    (func $main
         i32.const 5
	 i32.const 0
	 call $fact
    )
    (func $fact (param $n i32) (param $res i32) (result i32)
        i32.const 1
	local.set $res
        loop
	     local.get $n
	     i32.const 1
	     i32.le_u
	     if
	         br 1
	     else
	         local.get $n
		 local.get $res
		 i32.mul
		 local.set $res
		 local.get $n
		 i32.const 1
		 i32.sub
		 local.set $n
	     end
	end
	local.get $res
    )
)
	     
			
	      