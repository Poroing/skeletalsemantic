BASE
number32

FLOW
stack

PROGRAM
instructions : stack * stack

- constructors
const32 : number32 -> instructions
add32 : instructions
cons : instructions * instructions -> instructions
block : instructions -> instructions
break : number32 -> instructions

- filters
pop32 : stack -> stack * number32
push32 : stack * number32 -> stack
add32 : number32 * number32 -> number32
break : stack * number32 -> stack
isbreaking : stack -> unit
isnotbreaking : stack -> unit
decrementbreaking : stack -> stack
id : stack -> stack

- rules
Cons(cons x_t1 x_t2) = [H(x_s, x_t1, x_f); [| [isbreaking(x_f); id(x_f) ?> (x_o)] || [isnotbreaking(x_f); H(x_f, x_t2, x_o)] |]{x_o} ]
Const(const32 x_t) = [push32(x_s, x_t) ?> (x_o)]
Add(add32) = [pop32(x_s) ?> (x_f1, x_t1); pop32(x_t1) ?> (x_f2, x_t2); add32(x_t1, x_t2) ?> (x_t3); push32(x_f2, x_t3) ?> (x_o)]
Block(block x_t) = [H(x_s, x_t, x_f); [| [isbreaking(x_f); decrementbreaking(x_f) ?> (x_o)] || [isnotbreaking(x_f); id(x_f) ?> (x_o)] |]{x_o}]
