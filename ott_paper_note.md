#Ott Paper

##Objective

The objective of Ott is to provide an easy to use metalanguage to describe
semantices of language. The tool ott can compile the semantics to various
proof assistant code.

##Problem

There is no easy way to specify the semantic without a DSL. LaTeX description
is prone to errors. And proof assistant specification is hard to read and
not known by everyone.

##Description of the semantics and concrete syntax

An ott file describe the semantics of the language via an ASD and
rules. Somme annotation can be added to aid the translation into Proof
Assistant and LaTeX.

##Description of the syntax in Ott
It seems there is an interesting choice for the description of the syntax in
ott. The syntax described seems to be usable in the description of the semantic
of the language. (Necessary to more precisely 2.3.)

##Binding


##Compilation to Proof Assistant Code
A lot of notion are not known. What is subrule order ?
Strongly Connected component of rule dependancy are made a block of
mutually recursive.

A lot of implementation details.

What is a subgrammar, subgrammar predicate, subgrammar rule, non-free rules

The description of inference rules that take an arbitrary number of premises
(lists) needs a particular handling in the compilation.

##Case study

A part of ott has been implemented in ott. It is the same for a good part
of Ocaml.

##Related Work
Ott brought an ease to proove existing semantics (Comparison with Ocaml proofs).
Right something about it and how it translate for semantic language

#Future Work
Extend binding.


##What I think we should talk about in the bibliography.

- What Ott brought about in terms of language specification: easier
description of the semantics via a metalanguage. Because LaTeX is not error
prone and proof assistant specification are hard to read.

- Maybe look into the work on binding (I will re-read the part about it and
look into what this is all about.). Skeletal semantic doesn't give any way to
specify how things bind, the binding must be handled by the one who specifies
the semantics via environment (At least, that is what I understood). Ott is
doing otherwise, there is a specific structure in the language and some rules
to define how things bind in terms.
