open WebAssemblyRulesOut
open WebAssemblyInterpreter
open Source

let contents (o : 'a option) =
  match o with
  | None -> failwith "no contents"
  | Some c -> c

let rec append_spec_instr spec_instr next_instrs =
    match spec_instr.it with
    | Ast.Const l -> Const (l.it, next_instrs)
    | Ast.Test op -> Cunop (FLOW.TestOp op, next_instrs)
    | Ast.Compare op -> Cbinop (FLOW.RelOp op, next_instrs)
    | Ast.Unary op -> Cunop (FLOW.UnOp op, next_instrs)
    | Ast.Binary op -> Cbinop (FLOW.BinOp op, next_instrs)
    | Ast.Convert op -> Cunop (FLOW.CvtOp op, next_instrs)
    | Ast.Block (result_type, instrs) ->
        Block (
          FLOW.number_of_int (List.length result_type),
          contents (Interpreter.getInstructionConcatenation
            (contents (Interpreter.getInstructionConcatenation
              (transform_instr_list instrs)
              (End Nop)))
            next_instrs))
    | Ast.Loop (result_type, instrs) ->
        Loop (
          contents (Interpreter.getInstructionConcatenation
            (contents (Interpreter.getInstructionConcatenation
              (transform_instr_list instrs)
              (End Nop)))
            next_instrs))
    | Ast.If (result_type, then_instrs, else_instrs) ->
        If (
          FLOW.number_of_int (List.length result_type),
          contents (Interpreter.getInstructionConcatenation
            (transform_instr_list then_instrs)
            (Else (contents (Interpreter.getInstructionConcatenation
              (transform_instr_list else_instrs)
              (End next_instrs))))))
    | Ast.Br l ->
        Br (I32 l.it, next_instrs)
    | Ast.BrIf l ->
        BrIf (I32 l.it, next_instrs)
    | Ast.Return ->
        Return next_instrs
    | Ast.Call a ->
        Call (I32 a.it, next_instrs)
    | Ast.LocalGet id ->
        Getlocal (I32 id.it, next_instrs)
    | Ast.LocalSet id ->
        Setlocal (I32 id.it, next_instrs)
    | _ -> failwith "Not Implemented"

and transform_instr_list instr_list  =
  List.fold_right append_spec_instr instr_list Nop

let transform_func module_ func =
  let arity, return_arity =
    match Ast.func_type_for module_ func.it.Ast.ftype with
    | FuncType (input, output) -> List.length input, List.length output
  in
  {
    FLOW.func_instr = transform_instr_list func.it.body;
    FLOW.func_arity = arity;
    FLOW.func_return_arity = return_arity
  }

let transform_module_ module_ =
  Array.map (transform_func module_) (Array.of_list module_.it.funcs)

let transform_definition definition = 
  match definition.it with
  | Script.Textual module_ -> transform_module_ module_
  | _ -> failwith "Definition must be a module"

let transform_script script =
  match script.it with
  | [ Script.Module (_, definition) ] -> transform_definition definition
  | _ -> failwith "Script must be a module"


let get_list_functions_from_file file_name =
  let file_channel = open_in file_name in
  try
    let lexbuf = Lexing.from_channel file_channel in
    let var_opt, def = Parse.parse file_name lexbuf Parse.Module in
    close_in file_channel;
    transform_definition def
  with
  | Script.Syntax ({left={line;column};_}, s) ->
    close_in file_channel;
    Printf.printf "(line %i, column %i, %s)\n" line column s;
    failwith "Syntax Error"
  | exn ->
      close_in file_channel;
      raise exn


let () =
  let functions = get_list_functions_from_file "test.wat" in 
  let empty_state = Interpreter.empty_stack () in
  let empty_state = {empty_state with state_funcs = functions}
  in
  Interpreter.print_state
    (Interpreter.eval empty_state (Invoke (FLOW.number_of_int 0, Nop)))
