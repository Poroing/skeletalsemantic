import pygments.lexer
import pygments.token

class CustomLexer(pygments.lexer.RegexLexer):

    name='Wasm'
    aliases = ['wasm']
    filenames = ['*.wasm']

    tokens = {
        'root' : [
            (r'<.*>', pygments.token.Comment),
            (r'(i|f)(32|64)', pygments.token.Keyword),
            (r'\$\w+', pygments.token.Name),
            (r'[a-zA-Z]\w*', pygments.token.Name.Function),
            (r'\d*\.?\d+', pygments.token.Number),
            (r'\(|\)|\.', pygments.token.Punctuation),
            (r'\s', pygments.token.Whitespace)
        ]
    }
