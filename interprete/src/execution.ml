let is_block_entry (i : ASD.instr) =
  match i with
  | ASD.ControlInstr (ASD.Block _) -> true
  | ASD.ControlInstr (ASD.Loop _) -> true
  | ASD.ControlInstr (ASD.If _) -> true
  | _ -> false

let is_block_end (i : ASD.instr) =
  i = ASD.ControlInstr ASD.End

let rec get_instrs_in_and_after_block (prgrm : ASD.program) (block_depth : int) =
  if current_depth < 0 then
    [], prgrm
  else
    match prgrm with
    | [] -> failwith "Block is not terminated with an end"
    | first_instr :: next_instrs ->
        let next_depth =
          if is_block_entry first_instr then
            current_depth + 1
          else if is_block_end first_instr then
            current_depth - 1
          else
            current_depth
        in
        let next_instrs_in_block, intrs_after_block =
          get_instrs_in_and_after_block tl next_depth
        in
        first_instr :: next_instrs_in_block, intrs_after_block

let get_instrs_after_block (prgrm : ASD.program) (block_depth : int) =
  snd (get_instrs_in_and_after_block prgrm) block_depth

let get_instrs_in_block (prgrm : ASD.program) (block_depth : int) =
  fst (get_instrs_in_and_after_block l) block_depth

let  get_i32_leading_zeros_count (c : ASD.value) = 0 (* TODO: implement function *)
and get_i32_trailing_zeros_count (c : ASD.value) = 0 (* TODO: implement function *)
and get_i32_non_zeros_count (c : ASD.value) = 0 (* TODO: implement function *)
and is_i32_zero (c : ASD.value) =
  if c = 0 then 1 else 0
and are_equal (c1 : ASD.value) (c2 : ASD.value) =
  if c1 = c2 then 1 else 0

let get_iunop_function (op : ASD.iunop) =
  match op with
  | ASD.Clz -> get_i32_leading_zeros_count
  | ASD.Ctz -> get_i32_trailing_zeros_count
  | ASD.Popcnt -> get_i32_non_zeros_count
  | ASD.Eqz -> is_i32_zero

let get_ibinop_function (op : ASD.ibinop) =
  match op with
  | ASD.Add -> (+)
  | ASD.Mul -> ( * )
  | ASD.Eq -> are_equal
  | _ -> failwith "not implemented yet" (* TODO: implement other operators *)

let step_numeric_instr (program_tail : ASD.program) (s : Stack.stack) (numeric_instr : ASD.numericInstr) =
  match numeric_instr with
  | ASD.Const c -> program_tail, Stack.push s (Stack.I32 c)
  | ASD.IUnop op ->
     if (not (Stack.isTopI32 s)) then
       failwith "Top of stack must be i32"
     else
       let (stack_top, new_stack) = Stack.pop s in
       let c = match stack_top with
         | I32 c -> c
         | _ -> failwith "Unreachable"
       in
       program_tail, Stack.push new_stack (Stack.I32 ((get_iunop_function op) c))
  | ASD.IBinop op -> failwith "Not implemented"


let step_br (prgrm : ASD.program) (stck : Stack.stack) (deph : int) = 
  let lbl_arity, lbl_instrs = get_label stck depth in
  let values = get_n_values stck lbl_arity in
  let new_stack = push_values (pop_until_label stck depth) values



let step_control_instr (l : ASD.program) (s : Stack.stack) (control_instr : ASD.controlInstr) =
  match control_instr with
    Nop -> (l, s)
  | Block _x -> (l, Stack.push s (Label (1, [])))
  | Loop  t -> (l, Stack.push s (Label (0, get_instrs_in_block l 0)))
  | If t -> (l, s)
  | Else -> failwith "Else without if"
  | Br i -> (l, s)
  | Br_if i -> (l, s)
  | End -> (l, s)
  | Unreachable -> failwith "unreachable"


let step (l : ASD.program) (s : Stack.stack) =
  match l with
  | ASD.NumericInstr numeric_instr :: program_tail -> 
     step_numeric_instr program_tail s numeric_instr
