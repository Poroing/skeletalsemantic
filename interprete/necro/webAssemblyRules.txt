BASE
number
tunop
tbinop

FLOW
label
frame
func
state
stackValue
stackValueArray

PROGRAM
instructions : state * state
stackManipulation : state * state

- constructors
nop : instructions
const : number * instructions -> instructions
cunop : tunop * instructions -> instructions
cbinop : tbinop * instructions -> instructions
end : instructions -> instructions 

block : number * instructions -> instructions
loop : instructions -> instructions
if : number * instructions -> instructions
else : instructions -> instructions
br : number * instructions -> instructions
popNLabels : number -> stackManipulation
brIf : number * instructions -> instructions

getlocal : number * instructions -> instructions
setlocal : number * instructions -> instructions
invoke : number * instructions -> instructions
call : number * instructions -> instructions
return : instructions -> instructions

- filters
push : state * stackValue -> state
pop : state -> state * stackValue
numberToStackValue : number -> stackValue
isNumber : stackValue -> number
isLabel : stackValue -> label
binop : tbinop * number * number -> number
unop : tunop * number -> number
skip : state -> state
isTrue : number -> unit
isFalse : number -> unit

popNValues : state * number -> state * stackValueArray
pushNValues : state * stackValueArray -> state
popUntilLabel : state * number -> state
popUntilFrame : state -> state
popValuesBeforeLabelOrFrame : state -> state * stackValueArray
isFrameFirst : state -> unit
isLabelFirst : state -> unit
getLabelWithDepth : state * number -> label
getLabelArity : label -> number
getLabelInstructions : label -> instructions
getInstructionConcatenation : instructions * instructions -> instructions
getInstructionAfterBlock : instructions * number -> instructions
getInstructionInsideBlock : instructions * number -> instructions
splitInstructionsIf : instructions -> instructions * instructions
createLabel : number * instructions -> label
createZero : unit -> number
plusOne: number -> number
minusOne: number -> number
createEmptyLabel : number -> label
createLoopLabel : instructions -> label
labelToStackValue : label -> stackValue

createFrame : number * stackValueArray -> frame
isFrame : stackValue -> frame
getFrame : state -> frame
setFrame : state * frame -> state
getFrameArity : frame -> number
frameToStackValue : frame -> stackValue

getLocal : frame * number -> number
setLocal : frame * number * number -> frame

getFunctionAdress : frame * number -> number
getFunction : state * number -> func
getFunctionArity : func -> number
getFunctionReturnArity : func -> number
getFunctionInstructions : func -> instructions
getFunctionInstructionsConcatenation : number * instructions * instructions -> instructions
getInstructionsAfterCall : state * instructions -> instructions

- atoms
empty_stack : unit -> state

- rules

Const(const x_t1 x_t2) = [ numberToStackValue(x_t1) ?> (x_f1); push(x_s, x_f1) ?> (x_f2); H(x_f2, x_t2, x_o) ]

Binop(cbinop x_t1 x_t2) = [ pop(x_s) ?> (x_f1, x_f2); isNumber(x_f2) ?> (x_t3); pop(x_f1) ?> (x_f3, x_f4); isNumber(x_f4) ?> (x_t4); binop(x_t1, x_t4, x_t3) ?> (x_t5); numberToStackValue(x_t5) ?> (x_f5); push(x_f3, x_f5) ?> (x_f6); H(x_f6, x_t2, x_o) ]

Unop(cunop x_t1 x_t2) = [ pop(x_s) ?> (x_f1, x_f2); isNumber(x_f2) ?> (x_t3); unop(x_t1, x_t3) ?> (x_t4); numberToStackValue(x_t4) ?> (x_f3); push(x_f1, x_f3) ?> (x_f4); H(x_f4, x_t2, x_o) ]

Nop(nop) = [skip(x_s) ?> (x_o)]

Block(block x_t1 x_t2) = [ createEmptyLabel(x_t1) ?> (x_f1); labelToStackValue(x_f1) ?> (x_f2); push(x_s, x_f2) ?> (x_f3); H(x_f3, x_t2, x_o) ]

Loop(loop x_t1) = [ createZero() ?> (x_t2); getInstructionInsideBlock(x_t1, x_t2) ?> (x_t3); createLoopLabel(x_t3) ?> (x_f3); labelToStackValue(x_f3) ?> (x_f4); push(x_s, x_f4) ?> (x_f5); H(x_f5, x_t1, x_o) ]

If(if x_t1 x_t2) = [ pop(x_s) ?> (x_f2, x_f3); isNumber(x_f3) ?> (x_t4); createEmptyLabel(x_t1) ?> (x_f5); labelToStackValue(x_f5) ?> (x_f6); push(x_f2, x_f6) ?> (x_f7); createZero() ?> (x_t3); getInstructionInsideBlock(x_t2, x_t3) ?> (x_t8); getInstructionAfterBlock(x_t2, x_t3) ?> (x_t9); splitInstructionsIf(x_t8) ?> (x_t10, x_t11); [| [ isTrue(x_t4); getInstructionConcatenation(x_t10, x_t9) ?> (x_t12) ] || [ isFalse(x_t4); getInstructionConcatenation(x_t11, x_t9) ?> (x_t12) ] |]{x_t12}; H(x_f7, x_t12, x_o) ]

Br(br x_t1 x_t4) = [ getLabelWithDepth(x_s, x_t1) ?> (x_f2); getLabelArity(x_f2) ?> (x_t6); getLabelInstructions(x_f2) ?> (x_t3); popNValues(x_s, x_t6) ?> (x_f4, x_f5); H(x_f4, popNLabels x_t1, x_f6); pushNValues(x_f6, x_f5) ?> (x_f7); getInstructionAfterBlock(x_t4, x_t1) ?> (x_t5); getInstructionConcatenation(x_t3, x_t5) ?> (x_t8); H(x_f7, x_t8, x_o) ]

PopNLabels(popNLabels x_t1) = [ pop(x_s) ?> (x_f1, x_f2); [| [ isNumber(x_f2) ?> (x_t3); H(x_f1, popNLabels x_t1, x_o) ] || [ isLabel(x_f2) ?> (x_f3); [| [ isTrue(x_t1); minusOne(x_t1) ?> (x_t2); H(x_f1, popNLabels x_t2 , x_o) ] || [ isFalse(x_t1); skip(x_f1)  ?> (x_o) ] |]{x_o} ] |]{x_o} ]

BrIf(brIf x_t1 x_t3) = [ pop(x_s) ?> (x_f2, x_f3); isNumber(x_f3) ?> (x_t2); [| [ isTrue(x_t2); H(x_f2, br x_t1 x_t3, x_o) ] || [ isFalse(x_t2); H(x_f2, x_t3, x_o) ] |]{x_o} ]

End(end x_t2) = [ popValuesBeforeLabelOrFrame(x_s) ?> (x_f2, x_f3); pop(x_f2) ?> (x_f4, x_f6); pushNValues(x_f4, x_f3) ?> (x_f5); H(x_f5, x_t2, x_o) ] 

GetLocal(getlocal x_t1 x_t2) = [ getFrame(x_s) ?> (x_f1); getLocal(x_f1, x_t1) ?> (x_t3); numberToStackValue(x_t3) ?> (x_f3); push(x_s, x_f3) ?> (x_f2); H(x_f2, x_t2, x_o) ]

SetLocal(setlocal x_t1 x_t2) = [ getFrame(x_s) ?> (x_f1); pop(x_s) ?> (x_f2, x_f3); isNumber(x_f3) ?> (x_t3); setLocal(x_f1, x_t1, x_t3) ?> (x_f4); setFrame(x_f2, x_f4) ?> (x_f5); H(x_f5, x_t2, x_o) ]

Invoke(invoke x_t1 x_t2) = [ getFunction(x_s, x_t1) ?> (x_f1); getFunctionArity(x_f1) ?> (x_t3); getFunctionReturnArity(x_f1) ?> (x_t4); popNValues(x_s, x_t3) ?> (x_f2, x_f3); createFrame(x_t4, x_f3) ?> (x_f4); frameToStackValue(x_f4) ?> (x_f5); push(x_f2, x_f5) ?> (x_f6); getFunctionInstructions(x_f1) ?> (x_t5); getFunctionInstructionsConcatenation(x_t4, x_t5, x_t2) ?> (x_t6); H(x_f6, x_t6, x_o) ] 

Call(call x_t1 x_t2) = [ getFrame(x_s) ?> (x_f1); getFunctionAdress(x_f1, x_t1) ?> (x_t3); H(x_s, invoke x_t3 x_t2, x_o) ]

Return(return x_t1) = [ getInstructionsAfterCall(x_s, x_t1) ?> (x_t2); getFrame(x_s) ?> (x_f1); getFrameArity(x_f1) ?> (x_t3); popNValues(x_s, x_t3) ?> (x_f2, x_f3); popUntilFrame(x_f2) ?> (x_f4); pop(x_f4) ?> (x_f5, x_f7); pushNValues(x_f4, x_f3) ?> (x_f6); H(x_f6, x_t2, x_o) ]
