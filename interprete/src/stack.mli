type label = int*ASD.instr list
type stackValue = Label of label | I32 of ASD.value
type stack = stackValue list
val push : stack -> stackValue -> stackValue list
val pop : stack -> stackValue * stackValue list
val isTopLabel : stack -> bool
val isTopI32 : stack -> bool
