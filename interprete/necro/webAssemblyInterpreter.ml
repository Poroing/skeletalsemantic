open WebAssemblyRulesOut

module FLOW = struct
  type number = Values.value
  type tunop = 
    | UnOp of Ast.unop 
    | CvtOp of Ast.cvtop
    | TestOp of Ast.testop
  type tbinop =
    | BinOp of Ast.binop
    | RelOp of Ast.relop
  type frame = {
    frame_arity : int;
    frame_params : number array
  }
  type stackValue =
    | Label of label
    | Number of number
    | Frame of frame
  and stackValueArray = stackValue list
  and stack = stackValue list
  and state = {
    state_stack : stack;
    state_funcs : func array
  }
  and label = {
    label_instr :
      (number, tunop, tbinop, label, frame, func, state, stackValue, stackValueArray)
      instructions;
    label_arity : int
  }
  and func = {
    func_instr :
      (number, tunop, tbinop, label, frame, func, state, stackValue, stackValueArray)
      instructions;
    func_arity : int;
    func_return_arity : int
  }

  let get_next_instructions is =
    match is with
    | Nop -> failwith "Can't get next instructions of Nop"
    | Const (_, i) -> i
    | Cunop (_, i) -> i
    | Cbinop (_, i) -> i
    | Block (_, i) -> i
    | Loop i -> i
    | If (_, i) -> i
    | Else i -> i
    | Br (_, i) -> i
    | BrIf (_, i) -> i
    | End i -> i
    | Getlocal (_, i) -> i
    | Setlocal (_, i) -> i
    | Invoke (_, i) -> i
    | Call (_, i) -> i
    | Return i -> i
    | _ -> failwith "get_next_instructions was not given an instructions"

  let set_next_instructions is new_instructions =
    match is with
    | Nop -> failwith "Can't set next instructions of Nop"
    | Const (n, _) -> Const (n, new_instructions)
    | Cunop (op, _) -> Cunop (op, new_instructions)
    | Cbinop (op, _) -> Cbinop (op, new_instructions)
    | Block (arity, _) -> Block (arity, new_instructions)
    | Loop _ -> Loop new_instructions
    | If (arity, _) -> If (arity, new_instructions)
    | Else _ -> Else new_instructions 
    | Br (depth, _) -> Br (depth, new_instructions)
    | BrIf (depth, _) -> BrIf (depth, new_instructions)
    | End _ -> End new_instructions
    | Getlocal (id, _) -> Getlocal (id, new_instructions)
    | Setlocal (id, _) -> Setlocal (id, new_instructions)
    | Invoke (id, _) -> Invoke (id, new_instructions)
    | Call (id, _) -> Call (id, new_instructions)
    | Return _ -> Return new_instructions
    | _ -> failwith "set_next_instructions was not given an instructions"

  let is_block_entry i =
    match i with
    | Block _ -> true
    | Loop _ -> true
    | If _ -> true
    | _ -> false

  let int_of_number (n : number) =
    match n with
    | Values.I32 i -> Some (Int32.to_int i)
    | Values.I64 i -> Some (Int64.to_int i)
    | _ -> None

  let number_of_int (i : int) =
    Values.I32 (Int32.of_int i)

  let empty_stack () = {
    state_stack = [];
    state_funcs = Array.make 0 {func_instr=Nop;func_arity=0;func_return_arity=0}
  } 

  let string_of_number (n : number) =
    Values.string_of_value n

  let string_of_label (l : label) =
    Printf.sprintf "Label(arity=%i)" l.label_arity

  let string_of_frame (f : frame) =
    Printf.sprintf "Frame(arity=%i)" f.frame_arity

  let string_of_stackvalue (v : stackValue) =
    match v with
    | Number n -> string_of_number n
    | Label l -> string_of_label l
    | Frame f -> string_of_frame f

  let print_state (st : state) =
    Printf.printf "[";
    match st.state_stack with
    | [] -> ()
    | v :: t ->
        begin
          Printf.printf "%s" (string_of_stackvalue v);
          List.iter
            (fun y -> Printf.printf ", %s" (string_of_stackvalue y))
            t
        end;
    Printf.printf "]"

  let isNumber (v : stackValue) =
    match v with
    | Number n -> Some n
    | _ -> None

  let isLabel (v : stackValue) =
    match v with
    | Label l -> Some l
    | _ -> None

  let isFrame (v : stackValue) =
    match v with
    | Frame f -> Some f
    | _ -> None

  let numberToStackValue (n : number) =
    Some (Number n)

  let labelToStackValue (l : label) =
    Some (Label l)

  let push (st : state) (v : stackValue) =
  Some ({st with state_stack = v :: st.state_stack})

  let pushNValues (st : state) (vs : stackValueArray) =
    (* TODO: Make the links with push more visible *)
    Some ({st with state_stack = List.rev vs @ st.state_stack})

  let pop (st : state) =
    match st.state_stack with
    | [] -> None
    | v :: t -> Some ({st with state_stack = t}, v)

  let isTrue (n : number) =
    match int_of_number n with
    | Some 0 -> None
    | None -> None
    | _ -> Some ()

  let isFalse (n : number) =
    match int_of_number n with
    | Some 0 -> Some ()
    | _ -> None

  let rec popNValues (st : state) (n : number) = 
    let rec aux (st : state) (n : int) =
      if n <= 0 then
        Some (st, []) 
      else
        match pop st with
        | None -> None
        | Some (new_stack, v) ->
            match aux new_stack (n - 1) with
            | None -> None
            | Some (final_stack, vs) -> Some (final_stack, v :: vs)
    in
    match int_of_number n with
    | None -> None
    | Some i -> aux st i

  let rec isFrameFirst (state : state) =
    match pop state with
    | None -> None
    | Some (t, v) ->
        match isFrame v, isLabel v with
        | None, None -> isFrameFirst t
        | Some _, _ -> Some ()
        | _ -> None

  let rec isLabelFirst (state : state) =
    match pop state with
    | None -> None
    | Some (t, v) ->
        match isFrame v, isLabel v with
        | None, None -> isFrameFirst t
        | _, Some _ -> Some ()
        | _ -> None

  let popUntilLabel (st : state) (depth : number) =
    let rec aux (st : state) (depth : int) =
      match pop st with
      | None -> None
      | Some (t, v) ->
        match isLabel v with
        | None -> aux t depth
        | _ ->
            if depth <= 0 then
              Some st
            else
              aux t (depth - 1)
    in
    match int_of_number depth with
    | None -> None
    | Some i -> aux st i

  let rec popValuesBeforeLabelOrFrame (st : state) =
    match pop st with
    | None -> None
    | Some (t, v) ->
      match isLabel v, isFrame v with
      | None, None -> begin
          match popValuesBeforeLabelOrFrame t with
          | Some (t, vs) -> Some (t, v :: vs)
          | None -> None
        end
      | _ -> Some (st, [])

  let rec popUntilFrame (st : state) =
    match pop st with
    | None -> None
    | Some (t, v) ->
        match isFrame v with
        | None -> popUntilFrame t
        | Some _ -> Some st

  let rec popUntilFrameOrLabel (st : state) =
    match pop st with
    | None -> None
    | Some (t, v) ->
        match isFrame v with
        | Some _ -> Some st 
        | None ->
            match isLabel v with
            | None -> popUntilFrameOrLabel t
            | Some _ -> Some st

  let getLabelWithDepth (st : state) (n : number) =
    let rec aux (st : state) (n : int) =
      match pop st with
      | None -> None
      | Some (t, v) ->
          match isLabel v with
          | None -> aux t n
          | Some l ->
              if n <= 0 then
                Some l
              else
                aux t (n - 1)
    in
    match int_of_number n with
    | None -> None
    | Some i -> aux st i

  let getLabelArity (l : label) =
    Some (number_of_int l.label_arity)

  let getLabelInstructions (l : label) =
    Some l.label_instr

  let getInstructionConcatenation lhs rhs =
    let rec aux lhs rhs =
      match lhs with
      | Nop -> rhs
      | _ ->
            set_next_instructions
              lhs
              (aux (get_next_instructions lhs) rhs)
      in
      Some (aux lhs rhs)

  let getInstructionAfterBlock instr (n : number) =
    let rec aux instr (depth : int) =
      match instr with
      | _ when is_block_entry instr ->
        aux (get_next_instructions instr) (depth + 1)

      | End next_instructions ->
          if depth <= 0 then
            next_instructions
          else
            aux next_instructions (depth - 1)
      | _ -> aux (get_next_instructions instr) depth
    in
    match int_of_number n with
    | None -> None
    | Some i -> Some (aux instr i)

  let getInstructionInsideBlock instr (n : number) =
    let rec aux instr (depth : int) =
      match instr with
      | _ when is_block_entry instr ->
        set_next_instructions
          instr
          (aux (get_next_instructions instr) (depth + 1))
      | End next_instructions when depth <= 0 ->
          End Nop
      | End next_instructions ->
          End (aux next_instructions (depth - 1))
      | _ ->
          set_next_instructions
            instr
            (aux (get_next_instructions instr) depth)
    in
    match int_of_number n with
    | None -> None
    | Some i -> Some (aux instr i)

  let rec getInstructionsAfterCall (st : state) instr =
    match instr with
    | End next_instructions -> begin
        match popUntilFrameOrLabel st with
        | None -> None
        | Some next_state -> 
          match pop next_state with
          | None -> None
          | Some (t, v) ->
              match isFrame v with
              | None -> getInstructionsAfterCall t next_instructions
              | Some _ -> Some next_instructions
    end
    | _ -> getInstructionsAfterCall st (get_next_instructions instr)

  let splitInstructionsIf instr =
    let rec aux instr (depth : int) =
      match instr with
      | Else next_instructions when depth <= 0 ->
          (End Nop, next_instructions)
      | End next_instructions ->
        let before_else, after_else =
          aux next_instructions (depth - 1)
        in
        End before_else, after_else
      | _ when is_block_entry instr ->
        let before_else, after_else =
          aux (get_next_instructions instr) (depth + 1)
        in
        set_next_instructions instr before_else, after_else
      | _ ->
          let before_else, after_else =
            aux (get_next_instructions instr) depth
          in
          set_next_instructions instr before_else, after_else
    in
    Some (aux instr 0)

  let getFunctionInstructionsConcatenation
      (arity : number)
      function_instructions
      next_instructions =
    match
      getInstructionConcatenation
        function_instructions
        (End (End next_instructions))
    with
    | None -> None
    | Some i -> Some (Block (arity, i))


  let createLabel (n : number) instr =
    match int_of_number n with
    | None -> None
    | Some i -> Some {label_instr=instr; label_arity=i}

  let createEmptyLabel (n : number) =
    createLabel n Nop

  let createLoopLabel instr =
    createLabel (number_of_int 0) instr

  let getFunctionInstructions {func_instr; _} =
    Some func_instr

  let getFunctionReturnArity {func_return_arity; _} =
    Some (number_of_int func_return_arity)

  let getFunctionArity {func_arity; _} =
    Some (number_of_int func_arity)

  let getFunction {state_funcs; _} (adress : number) =
    match int_of_number adress with
    | None -> None
    | Some i -> Some (Array.get state_funcs i)

  let getFunctionAdress (_ : frame) (module_adress : number) =
    Some module_adress

  let setLocal (frame : frame) (id : number) (value : number) =
    match int_of_number id with
    | None -> None
    | Some i ->
      let new_frame = {frame with frame_params = Array.copy frame.frame_params}
      in
      Array.set new_frame.frame_params i value;
      Some new_frame

  let getLocal {frame_params; _} (id : number) =
    match int_of_number id with
    | None -> None
    | Some i -> Some (Array.get frame_params i)

  let frameToStackValue (frame : frame) =
    Some (Frame frame)

  let getFrameArity {frame_arity; _} =
    Some (number_of_int frame_arity)

  let rec setFrame (state : state) (frame : frame) =
    match pop state with
    | None -> None
    | Some (t, v) ->
        match isFrame v with
        | None -> begin
            match setFrame t frame with
            | None -> None
            | Some new_state -> push new_state v
        end
        | Some _ ->
            match frameToStackValue frame with
            | None -> None
            | Some frame_value -> push t frame_value

  let rec getFrame (state : state) =
    match pop state with
    | None -> None
    | Some (t, v) ->
        match isFrame v with
        | None -> getFrame t
        | Some f -> Some f

  let numberListOfStackValueArray (values : stackValueArray) =
    let rec aux values =
      match values with
      | [] -> Some []
      | v :: t ->
          match isNumber v with
          | None -> None
          | Some n ->
            match aux t with
            | None -> None
            | Some tn -> Some (n :: tn)
    in
    aux values

  let createFrame (arity : number) (params : stackValueArray) =
    match int_of_number arity, numberListOfStackValueArray params with
    | None, _
    | _, None -> None
    | Some i, Some nl -> Some {
      frame_arity = i;
      frame_params = Array.of_list nl;
  }

  let createZero () =
    Some (number_of_int 0)

  let plusOne (n : number) =
    match int_of_number n with
    | None -> None
    | Some i -> Some (number_of_int (i + 1))

  let minusOne (n : number) =
    match int_of_number n with
    | None -> None
    | Some i -> Some (number_of_int (i - 1))

  let binop (op : tbinop) (lhs : number) (rhs : number) =
    try 
      let result =
        match op with
        | BinOp sub_op ->
            Eval_numeric.eval_binop sub_op lhs rhs
        | RelOp sub_op ->
            Values.value_of_bool (Eval_numeric.eval_relop sub_op lhs rhs)
      in
      Some result
    with 
      _ -> None

  let unop (op : tunop) (rhs : number) =
    try
      let result = 
        match op with
        | UnOp sub_op ->
            Eval_numeric.eval_unop sub_op rhs
        | CvtOp sub_op ->
            Eval_numeric.eval_cvtop sub_op rhs
        | TestOp sub_op ->
            Values.value_of_bool (Eval_numeric.eval_testop sub_op rhs)
      in
      Some result
    with
      _ -> None

  let skip (st : state) =
    Some st
end

module Interpreter = MakeInterpreter (FLOW)
