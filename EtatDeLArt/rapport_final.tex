\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.


\usepackage[utf8]{inputenc}%
\usepackage[main=english]{babel}

\usepackage{natbib}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{todonotes}
\usepackage{minted}
\usemintedstyle{bw}
\usepackage{listings}

\hyphenation{Web-As-sem-bly}   

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}
  
\title{SKEL}
%\thanks{Identify applicable funding agency here. If none, delete this.}

\author{\IEEEauthorblockN{Guillaume Barbier \quad\quad\quad Maxime Bridoux \quad\quad\quad Jean Jouve \quad\quad\quad Clément Legrand-Duchesne}
    \\\
    \IEEEauthorblockA{\textit{Univ Rennes, F-35000 Rennes, France}}}
    
    
%dept. name of organization (of Aff.)} \\


\newcommand{\type}[1]{\mathrm{#1}}
\newcommand{\ttrue}{\lstinline|true|}
\newcommand{\tfalse}{\lstinline|false|}
\newcommand{\twhile}{\lstinline|while|\;}
\newcommand{\tdo}{\;\lstinline|do|\;}
\newcommand{\texn}{\lstinline|exn|}
\newcommand{\tif}{\lstinline|if|\;}
\newcommand{\tthen}{\;\lstinline|then|\;}
\newcommand{\telse}{\;\lstinline|else|\;}

\maketitle

\begin{abstract}
Programming languages are often complex. Therefore, mathematical
models have been designed to guarantee their well-functioning. These
models are called semantics. We will be taking a closer look at how to
express semantics, before using meta-languages, that are languages
designed to describe semantics. Skeletal Semantics is a case in
point. This ongoing project at Inria produces a syntactic formulation
for a language that can be interpreted to produce rules for several
models of semantics. Our goal this year will be to write down the
syntactic description of the WebAssembly programming language using
Skeletal Semantics.
\end{abstract}

\begin{IEEEkeywords}
semantics, meta-language, Skeletal Semantics, WebAssembly
\end{IEEEkeywords}

\section{Introduction}
Possessing the complete specification of a programming language is
tantamount to knowing what a program does. If some aspects of the
behavior of the programming language are left in the dark, it is
impossible to guarantee the well-functioning of a program, let alone
precisely knowing what it does. The only way left to understand what
this program does on a given input is to execute it. For this reason,
having a strictly specified semantics of a construct language makes
programs safer and avoids a lot of errors and bugs due to the lack of
knowledge on the behavior of the language.

There are many programming languages such as Javascript or C that have
a detailed and precise semantics. An informal, yet very precise and
exhaustive specification of Javascript is given in ECMAScript
\citep{ecmascript2011} and is regularly updated. The book The C
programming Language written by \citet{cprogramminglanguage} was
considered to be a reference on how to program in C and thus used as
an informal specification of the language. However, its lack of
precision led to a great variety of different implementations of the C
language, a standardization was therefore necessary. The American
National Standards Institute thus published the standard ANSI C,
detailing the specification of C89.

However, some programming languages like R and Python do not have a
properly defined specification since this one is only given by the
implementation of the interpreter. As there might be different
interpreters for a single programming language, there is no guarantee
that the same program has an identical behavior on all these
interpreters. For this reason, it is not possible to talk about a
specification of the Python language, since the only way of giving a
meaning to a program is to execute it. There is only a reference
manual, that describes how programs should be written and that the
results might not be the same depending on the interpreter
used. Likewise, the semantics of OCaml is only defined by the
implementation of the interpreter and of the compiler, and there is no
guarantee that they both give the same meaning to a program.

This stemmed the development of precise mathematical models for
construct languages. These models, called semantics, describe the
processes a computer follows when executing a program in a specific
language. Defining the semantics of a language with precision gives a
specification of the language and allows us to do verification and
static analysis. However, there are many ways to describe a language
and thus a lot of ways of expressing semantics. To simplify this work,
some languages have been designed with the sole purpose of describing
the semantics of other languages. These languages are called
meta-languages.

Some meta-languages do more than just providing a model for other
construct languages. Their specification of other languages can be
interpreted, producing tools such as verifiers but also
interpreters. Being able to write down specifications with such
meta-languages can thus be a very interesting way to produce powerful
verified tools. The Skeletal Semantics meta-language, an ongoing
project at Inria, has this ability. Our goal for this year is to write
the specification of the WebAssembly language using Skeletal Semantics
to see how powerful this meta-language can be.

As a consequence, we will first highlight in
\autoref{sec:expressingsemantics} the major models used to express the
different semantics and their strength and weaknesses. We will take a
closer look in \autoref{sec:metalanguage} at some meta-languages: how
they were designed, their features. As a conclusion, we will detail
our goal for this year's project in \autoref{sec:goal}, specifying
WebAssembly using Skeletal Semantics.



\section{Expressing semantics}
\label{sec:expressingsemantics}

There are several ways to describe the semantics of a
program. Denotational semantics represents the program with a
mathematical function from the inputs to the output. However, this
model does not show how the calculations are performed. On the
contrary operational semantics describes how a valid program is
interpreted as sequences of computational steps. We chose to focus on
operational semantics for their level of detail. A step of computation
is a modification of a state of the program. This step can be
described with an inference rule, which states under which conditions
a transition can occur from a state to another one. An execution of a
program can then be modelled by deriving inference rules, each of them
matching a certain computational step.

Given an environment $\rho$ and an expression $e$, if evaluating $e$
in $\rho$ returns the value $v$ and changes the environment to
$\rho'$, the corresponding operation is denoted by:
\begin{equation}
    \rho \vdash e \Rightarrow \rho', v.
\end{equation}

An inference rule is composed of a set of premises (represented above
the horizontal line) and a conclusion (represented below the
horizontal line). The inference rule \ref{seq} depicts the step of
computation corresponding to a sequence of two operations.

\begin{equation}
  \label{seq}
  \begin{array}{c}
    \rho \vdash s_1 \Rightarrow \rho', () \quad \rho' \vdash s_2 \Rightarrow \rho'', v\\
    \hline
    \rho \vdash s_1 ;; s_2 \Rightarrow \rho'', v
  \end{array}
\end{equation}

 This rule means that if executing $s_1$ in the environment $\rho$
 yields the environment $\rho'$ and executing $s_2$ in the environment
 $\rho'$ yields $v$ in the environment $\rho''$ then executing the
 sequence $s_1 ;; s_2$ in $\rho$ yields $v$ in the environment
 $\rho''$.

\subsection{\emph{Big-step}}

\emph{Big-step} semantics, introduced by \citet{kahnNaturalSemantic}
and also known in the literature as natural semantics, is the first
operational semantics. The main idea is to consider a program as a
relation between its inputs and output: the meaning of an expression
is given by its final value. The evaluation of an expression matches
inference rules that require the computation of sub-expressions before
applying any inference rule. \emph{Big-step} semantics are often
easier to understand, since it highlights the structure of the
program, and gives an overview of the execution.

The usual natural semantics of the \tif construct is given by the two
inference rules \ref{bigStepIfTrue} and \ref{bigStepIfFalse}. Both
represent the execution of an \tif statement, the first one where the
condition is \ttrue, the other one where the condition is \tfalse. The
two rules are indeed very similar.

%true
\begin{equation}
  \label{bigStepIfTrue}
  \begin{array}{c}
    \rho \vdash E_1 \Rightarrow \rho', \ttrue \quad \rho' \vdash E_2 \Rightarrow \rho'', v_2\\
    \hline
    \rho \vdash \tif E_1 \tthen E_2 \telse E_3 \Rightarrow \rho'', v_2
  \end{array}
\end{equation}

%false
\begin{equation}
  \label{bigStepIfFalse}
  \begin{array}{c}
    \rho \vdash E_1 \Rightarrow \rho', \tfalse \quad \rho' \vdash E_3 \Rightarrow \rho'', v_3\\
    \hline
    \rho \vdash \tif E_1 \tthen E_2 \telse E_3 \Rightarrow \rho'', v_3
  \end{array}
\end{equation}

However, many concepts can not be described in \emph{big-step}, such
as non-terminating executions. It is indeed impossible to build the
inference tree of a program that does not terminate, since there is no
execution with a final state. For this reason it is also impossible to
distinguish correct but potentially non-terminating programs from
incorrect ones.

This idea of skipping intermediate states of an execution of a program
also forbids the analysis of parallel executions. Indeed, the final
result of two concurrent programs will be modelled as the pair of
values resulting from the execution of each program: the order of
evaluation of the sub-expressions is not specified. The next equation,
inference rule \ref{parrallelism} represents this parallel execution.

\begin{equation}
  \label{parrallelism}
  \begin{array}{c}
    \rho \vdash E_1 \Rightarrow \rho_1, v_1 \quad \rho \vdash E_2 \Rightarrow \rho_2, v_2\\
    \hline
    \rho \vdash (E_1||E_2) \Rightarrow \rho', (v_1||v_2)
  \end{array}
\end{equation}

In this particular example, the resulting environment $\rho'$ can not
be trivially obtained without detailing smaller steps of
computation. This is the major drawback of \emph{big-step}
semantics. Another drawback is the difficulty to implement these rules
in practice: indeed, if the language includes commands breaking the
control flow (such as breaks, call/cc and exceptions), it is necessary
to duplicate the inference rules to manage the different cases
resulting of the evaluation of the expressions, which leads to a great
amount of very similar rules for the same expression pattern. There
generally is a duplication for each sub-expression, as it can be seen
in the inference rules \ref{exception}.

\begin{equation}
    \label{exception}
    \begin{gathered}
        \begin{array}{c}
            \rho \vdash E_1 \Rightarrow^{exn} \\
            \hline
            \rho \vdash E_1 \lstinline|+| E_2 \Rightarrow ^{exn}
        \end{array} \\
        \begin{array}{c}
            \rho \vdash E_2 \Rightarrow^{exn} \\
            \hline
            \rho \vdash E_1 \lstinline|+| E_2 \Rightarrow^{exn}
        \end{array}
    \end{gathered}
\end{equation}

\subsection{\emph{Small-step}}

\emph{Small-step} semantics was introduced much later by
\citet{plotkinOperationalSemantic}. This semantics models the behavior
of the language by describing minimal steps of computation. At each
step, the semantics considers a term to evaluate as well as the
current state of the memory. From this term and this state it computes
a new term and a new state based on a sequence of applicable inference
rules. For instance, the \emph{small-step} semantics for the if
construct has two similar rules to the \emph{big-step} ones (rules
\ref{smallStepIfTrue} and \ref{smallStepIfFalse}) but also adds a new
rule, rule \ref{smallStepIfCond}, to reduce the conditional term to a
known value.

% true
\begin{equation}
  \label{smallStepIfTrue}
  \begin{array}{c}
    \hline
    \rho \vdash \tif \ttrue \tthen E_2 \telse E_3 \Rightarrow \rho' \vdash E_2
    \end{array}
\end{equation}

%false
\begin{equation}
  \label{smallStepIfFalse}
  \begin{array}{c}
    \hline
    \rho \vdash \tif \tfalse \tthen E_2 \telse E_3 \Rightarrow \rho' \vdash E_3
    \end{array}
\end{equation}

%reduce
\begin{equation}
  \label{smallStepIfCond}
  \begin{array}{c}
    \rho \vdash E_1 \Rightarrow \rho' \vdash E_1'\\
    \hline
    \rho \vdash \tif E_1 \tthen E_2 \telse E_3 \Rightarrow \rho' \vdash \tif E_1' \tthen E_2 \telse E_3
    \end{array}
\end{equation}

Indeed, contrary to \emph{big-step} semantics which would completely
evaluate the term $E_1$ to either $\ttrue$ or $\tfalse$,
\emph{small-step} semantics will detail all the evaluation of $E_1$,
step by step. Since the derivation of inference rules describes each
step of the evaluation of the program instead of evaluating the whole
program at once, it is now possible to consider an execution order,
thus giving a meaning to interlacing in parallelism. Let us take the
example of a parallel execution (see rules \ref{smallStepParallel1}
and \ref{smallStepParallel2}), where $a||b$ represents the parallel
execution of $a$ and $b$.

%parallelism_1
\begin{equation}
  \label{smallStepParallel1}
  \begin{array}{c}
    \rho \vdash E_1 \Rightarrow \rho' \vdash E_1'\\
    \hline
    \rho \vdash (E_1 || E_2) \Rightarrow \rho' \vdash (E_1' || E_2)
    \end{array}
\end{equation}

%parallelism_2
\begin{equation}
  \label{smallStepParallel2}
  \begin{array}{c}
    \rho \vdash E_2 \Rightarrow \rho' \vdash E_2'  \\
    \hline
    \rho \vdash (E_1 || E_2) \Rightarrow \rho' \vdash (E_1 || E_2')
    \end{array}
\end{equation}

There is now the choice to either compute a minimal step from the
first or the second term, until both of them are finally computed.

In a \emph{small-step} semantics, the $\twhile$ construct is expected
to be evaluated by progressively reducing the condition before
evaluating the body. However, the initial condition term would be lost
and could not be evaluated again in a new environment after looping in
the $\twhile$. For this reason, the $\twhile$ construct is transformed
to an $\tif$ construct followed by the same while construct (see rule
\ref{smallStepWhile}).

\begin{equation}
  \label{smallStepWhile}
  \begin{array}{c}
    \\
    \hline
    \rho \vdash \twhile E_1 \tdo E_2 \Rightarrow \\
    \rho, \tif E_1 \tthen E_2 \twhile E_1 \tdo E_2 \telse ()
    \end{array}
\end{equation}

This trick used to represent the $\twhile$ construct brings up a
totally new problem: the creation of virtual sub-terms. It is indeed
necessary at some steps to compute a new term that has no real
meaning. This gives an unnatural evaluation of some terms, that does
not reflect the reality.

Non-terminating programs can also be described in a \emph{small-step}
semantics. Indeed, the program can be represented by an endless
sequence of operations, whereas in \emph{big-step semantics}, a final
state is necessary to build the inference tree, and such a state does
not exist.

\subsection{\emph{Pretty-big-step}}

\emph{Big-step} and \emph{small-step} semantics have
drawbacks. Several attempts have been made to improve them or develop
alternatives. This is the case of \emph{pretty-big-step} semantics,
introduced by \citet{chargueraudPrettyBigStep} to improve
\emph{big-step} semantics. Recall that \emph{big-step} semantics
duplicates a lot of inference rules when introducing constructs that
break the control flow. In such cases, \emph{pretty-big-step}
decreases the number of rules through a decomposition of the original
rules of \emph{big-step} semantics into simpler atomic rules to enable
factorization. It also changes the values that expression can be
evaluated to. As an example, the $\twhile$ continuation rules in
\emph{pretty-big-step} semantic are as follows:

\begin{equation}
  \label{prettyBigStepWhileTo1}
  \begin{array}{c}
  \rho \vdash E_1 \Rightarrow \rho', v \quad \rho' \vdash v, \lstinline|while|_1 E_1 \tdo E_2 \Rightarrow
      \rho'', () \\
  \hline
  \rho \vdash \twhile E_1 \tdo E_2  \Rightarrow \rho'', ()
      \end{array}\text{;}
\end{equation}
\begin{equation}
  \label{prettyBigStepWhile1false}
  \begin{array}{c}
    \hline
    \rho \vdash \tfalse, \lstinline|while|_1 \; E_1 \tdo E_2 \Rightarrow \rho, ()
  \end{array}\text{;}
\end{equation}
\begin{equation}
  \label{prettyBigStepWhile1true}
    \begin{array}{c}
        \rho \vdash E_2 \Rightarrow \rho', () \quad \rho \vdash \rho', \lstinline|while|_2 \; E_1 \tdo E_2 \Rightarrow \rho'', () \\
        \hline
        \rho \vdash \ttrue, \lstinline|while|_2 \; E_1 \tdo E_2 \Rightarrow \rho'', ()
    \end{array}\text{;}
\end{equation}
\begin{equation}
  \label{prettyBigStepWhile2}
    \begin{array}{c}
    \rho \vdash \twhile E_1 \tdo E_2 \Rightarrow \rho', () \\
    \hline
    \rho'' \vdash \rho, \lstinline|while|_2 \; E_1 \tdo E_2 \Rightarrow \rho', ()
    \end{array}\text{.}
\end{equation}

There are two more rules compared to the specification of $\twhile$ in
\emph{big-step} semantics, and two term constructors have appeared:
$\lstinline|while|_1$ and $\lstinline|while|_2$. The main idea is to
describe more steps in the computation by keeping $E_1$ along the
derivation tree even after evaluating it, which solves the main issue
with \emph{small-step} semantics (not being able to reevaluate $E_1$
after a $\twhile$ loop). When building the derivation tree of a
$\twhile$ statement, rule \ref{prettyBigStepWhileTo1} applies. Let's
suppose we evaluated $E_1$, we now have to take a closer look at the
$\lstinline|while|_1$ constructor representing the $\twhile$ statement
with the evaluated condition. If the condition is $\tfalse$, rule
\ref{prettyBigStepWhile1false} applies and the loop ends. If the
condition remains $\ttrue$, rule \ref{prettyBigStepWhile1true} applies
and the loop keeps on, using a second constructor,
$\lstinline|while|_2$, that keeps the new environment after executing
$E_2$. The last rule \ref{prettyBigStepWhile2} calls the original
$\twhile$ to loop.

If exceptions were to be introduced in \emph{big-step} semantics,
three rules would have to be added, one for each sub-term and another
for the recursion. Here, adding the following rule \ref{pbsexception}
is enough.

\begin{equation}
    \label{pbsexception}
    \begin{array}{c}
        \hline
        \rho \vdash exn, t \Rightarrow \rho, exn
    \end{array}
\end{equation}

When only the $\twhile$ is specified, we have the same number of rules
in the two semantics. But in practice, there exist rules with a much
greater number of premises, rules for which the number of duplications
would be tremendous. However, only one rule needs to be added to
specify exceptions using \emph{pretty-big-step} semantics.

\section{Meta-language}
\label{sec:metalanguage}

Since each semantics has its own strengths and weaknesses, different
situations require different semantics. As a consequence, some
developers sometimes have to completely rewrite the specification of
their language as their needs vary. This was the case for CompCert, a
Coq certified C compiler. Its certification was originally written as
a \emph{big-step} semantics but the developers had to change to
\emph{small-step} semantics and thereby rewrite the whole
certification. This pinpoints the need to unify the different types of
semantics.

Furthermore, the distinction between these types of semantics remains
theoretical. In practice, many variations occur, for instance
\emph{big-step} semantics are sometimes defined with a specification
of the order of evaluation of the different sub-terms. This example
shows that there are needs of unification that goes beyond simple
conventions in the theoretical domain.

To fulfill these needs, different models have been introduced to
specify semantics and to make these specifications more precise. As
these new introduced models usually consists in a syntactic
reformulation of the different existing semantic models, they are
called meta-languages: languages for describing semantics. The ideal
meta-language would verify some desirable properties, such as being
independent of the different types of semantic and from any
programming paradigm. It should also be as complete as possible by
supporting arbitrarily complex features (continuation, true
concurrency, non-determinism or pointers for instance). Finally, this
meta-language should also be executable, with tools able to generate
interpreters, compilers or verifiers. In this section, we will present
four meta-languages that propose a general formalism for writing
inference rules about construct languages in order to compare what
they can offer.

\subsection{Ott and Lem}

There are two main ways of writing semantics with
meta-languages. Meta-languages can be used to either write informal
mathematics about a given construct language, such as inference rules,
or specify more formal models using the structure of another proof
assistant (such as Isabelle/HOL or Coq) to write provable properties
on the language. Ott \citep{sewellOtt} is one of the first
meta-language specifically designed to export its own formal
definitions to other proofs assistants. Ott uses a syntax similar to
\LaTeX \ to define for instance types and structures that can
automatically be exported to some proof assistants instead of directly
writing the specification with the proof assistant. Furthermore,
inference rules can very easily be read and exported to \LaTeX.

As specifications written in a proof assistant language are usually
hard to read due to the complexity of proof assistants, it is much
simpler to specify a construct language using Ott to write different
properties. These structures can be exported to Isabelle/HOL or Coq to
be proved. It also reduces the risk of making mistakes while
translating a specification to any other proof assistant.

Ott was a good step forward in the description of semantics, yet not
capable of directly exporting properties nor proofs. This forces
large-scale projects which use different proof assistants to redefine
each needed property in different assistants, despite centralizing
definitions. It should also be noted that using the automatically
generated types and relations to describe properties could be tedious
as these types are not always intuitive. An example of Ott code is
provided in listing \autoref{fig:ottfile}, with the specification of
rules for the reduction of terms in lambda calculus.  This example
contains three inference rules for reducing lambda calculus'
terms. The first one depicts the application of a lambda abstraction
to a value, the second the reduction of the left term in an
application and the last the reduction of the right term in an
application.

\begin{listing}
  \begin{minted}[breaklines, fontsize=\footnotesize]{coq}
  defn
  t1 --> t2 :: ::reduce::'' by

    -------------------------- :: ax_app
    (l x.t12) v2 --> {v2/x}t12

    t1 --> t1'
    -------------- :: ctx_app_fun
    t1 t -->t1' t

    t1 --> t1'
    ------------- :: ctx_app_arg
    v t1 --> t1'
  \end{minted}
  \caption{Sample of an Ott file describing the semantics of the lambda calculus.\label{fig:ottfile}}
\end{listing}

The meta-language Lem \citep{mulliganLem} was introduced to remove the
lack of property definition capability from Ott, with hope to improve
portability. The design of Lem takes inspiration from both functional
programming languages and proof assistants. The Caml-like grammar
allows the use of higher order functions as well as recursion or
pattern matching. Lem also adds logic constructors such as universal
and existential quantification for directly writing lemmas or
properties in the specification, which can then be exported to proof
assistants, avoiding the shortcoming Ott encountered.

Lem was used to specify the semantics of C11 and C++11 concurrency
model \citep{battyLemConcurrency} and to specify the semantic of the
OCaml\_light language \citep{owenLemOcamllight}. However, Lem itself
is lacking a specification of its semantics. This makes the
verification of compilation from Lem to proof assistant code
impossible.

\subsection{$\mathbb{K}$}

$\mathbb{K}$ is a meta-language introduced to ease the definition of
semantics of concurrent programming languages, but also to define the
type systems and the formal analysis tools that apply to
them. $\mathbb{K}$ is an executable meta-language which uses rewriting
rules to define \emph{small-step} semantics.

A specification in $\mathbb{K}$ consists in a grammar of the terms of
the specified language, combined with a set of rewriting rules that
apply on them. A rewriting rule is a means to replace within a term
all the sub-terms that match a given pattern with another sub-term. A
set of rewriting rules can be non-deterministic, in fact, several
rules can be applied on the same term, and a rewriting rule can be
applied in different ways.

$\mathbb{K}$ distinguishes computational and structural
rules. Computational rules corresponds to a computing step, whereas
structural rules consists in reorganizing the current
configuration. The specificity of $\mathbb{K}$-rewriting rules is that
they carry information about the context in which a sub-term can or
cannot be replaced: some parts of the terms are called read-only parts
whereas others can be edited. The main advantage of defining a
read-only part is that several rules can be applied simultaneously, as
long as they do not write in the same part of the term. This
parallelism enhances the efficiency of the rewriting system of
$\mathbb{K}$, but also enables us to consider concurrent programs,
that share some data, with different access rights. $\mathbb{K}$ was
also designed to easily handle languages with commands that impact the
control flow (such as breaks, call/cc or exceptions).

A complete static semantics of the C language has been written with
$\mathbb{K}$ and a competitive model checker for Java 1.4 has been
designed using $\mathbb{K}$. Unfortunately, $\mathbb{K}$ is not really
appropriate to describe all languages since it is sometimes difficult
to define a semantics using only rewriting rules in a
\emph{small-step} style.

\subsection{SKEL}
\label{subsec:skel}

SKEL is a shortcut for Skeletal Semantics, a meta-language first
introduced by \citet{bodinSkeletalSemantic}. Unlike previous
meta-languages that would provide a syntax to express the semantics of
a construct language (for instance by writing rule-based operational
semantics), SKEL is a meta-language for writing skeletons. These
skeletons are not a semantics on their own, but a syntactic
reformulation of the behaviour of a language. They can then be
interpreted, which provides a systematic way of deriving semantic
judgments. Specifying a construct language with SKEL simply consists
in writing its skeleton, as many interpretations are already
available.

The skeleton gives the structure of the semantics and the
interpretations give the details. Let us take a closer look at the
syntax of the skeletons. Skeletons are made of bones, that consists in
either a hook, a filter or branches. All of them are abstractions of
the behaviour of the construct language. A hook consists in the
evaluation of a sub-term, and lets the semantics use recursion.

Filters represent the basic computations. For instance, a language
might introduce the sum of two integers. The skeletal semantics will
introduce a filter $Sum$ to represent this, but will not give the
details of its implementation. The semantics of the actual execution
of the program will define $Sum$ as the sum function of two
integers. However, it is possible to define an abstract semantics
which gives an approximation : the sum of two intervals will then be
an interval which contains all possible values accessible as the sum
of two integers of the input intervals. This kind of semantics is
usually used in model checking to verify that a program does not reach
an unwanted state. This definition of $Sum$ makes it possible to
create a completely different semantics without much additional work.

The third basic notion of SKEL are branches. Branches let the program
make choices and parallelism.  For example, an if statement will
create two branches. Those branches will join differently depending on
the condition. In the case of parallelism, several branches will be
joined in a certain way. The way branches are joined is defined by the
interpretation.


\section{Goal}
\label{sec:goal}

The aim of our project is to produce a precise and reusable semantics
for the WebAssembly programming language.

The language used for web development is mostly Javascript which is
slow to parse and execute compared to desktop application. Therefore,
the language WebAssembly has been created to provide near native
performance for web applications. WebAssembly is not a language for
programming but an intermediate language for compiling programs.  The
formal semantic of the language written in \LaTeX \ stands on one
page, which makes {WebAssembly} a very lightweight language and
therefore a easy language to translate into. It is already implemented
in the four most used browsers Mozilla Firefox, Google Chrome, Safari
and Internet Explorer.

Having a formal semantics of WebAssembly benefits the process of
creating a compiler for a language into {WebAssembly} as it not only
helps building the translation but it also guaranties its
correctness. It also describes exactly how WebAssembly works, which
makes the process of translating the construct language into
WebAssembly far easier than if there were no specification.

There already exists some semantic specification of WebAssembly but
most of them do not serve our goal. The OCaml specification is biased
by the fact that OCaml itself is not specified. There is an Isabelle
specification but Isabelle is quite complex and not oriented toward
semantic specification: it remains a proof assistant. This is why we
would like to create a specification of the WebAssembly language in a
specified and semantics oriented language.

For that purpose we could use a meta-language such as previously
described (\autoref{sec:metalanguage}) and write the whole
specification with this meta-language. We here focus on the specific
meta-language Skeletal Semantics (\autoref{subsec:skel}). Indeed,
Skeletal Semantics offers a simple way of expressing the a language's
semantics which could be easier to reason with. Moreover, this could
answer the following question: is Skeletal Semantics too simple to
model an industrial language ?

If one desires to write a specification for a given language with
Skeletal Semantics, they just have to define a skeletal semantics,
which is just a syntactic work. Our current goal is now to write down
this skeletal semantics for WebAssembly based on its complete
specification. Another tool called Necro can then build an interpreter
or a compiler from the user-written skeletal semantics.

\section*{Acknowledgment}
We would like to thank Alan Schmitt for the time he spent supervising
us, for his patience and for the fruitful help he provided us with.

\bibliographystyle{named}
\bibliography{bibliography}

\end{document}
