type label = int * ASD.instr list
type stackValue =
  | Label of label
  | I32 of ASD.value
type stack = stackValue list
           
let push (s : stack) (v : stackValue) =
  v :: s
  
let pop (s : stack) =
  (List.hd s, List.tl s)
  
let isTopLabel (s : stack) =
  match s with
  | Label _ ::_ -> true
  | _ -> false
       
let isTopI32 (s : stack) =
  match s with
  | I32 _::_ -> true
  | _ -> false 

let rec get_label (stck : stack) (depth : int) =
  if depth < 0 then
    failwith "Depth cannot be negative"
  else
    match stck with
    | Label l :: tl when depth = 0 -> l
    | Label l :: tl -> get_label stck (depth - 1)
    | _ :: tl -> get_label stck depth 
    | [] -> failwith "There is not sufficiently enough label in the stack"

let pop_until_label (stck : stack) (lbl_depth : int) =
  if lbl_depth < 0
    stck
  else
    let next_depth = 
      if isTopLabel stck then
        lbl_depth - 1
      else
        lbl_depth
    in
    let _, stck_tl = pop stck in
    pop_until_label stck_tl next_depth

let push_values (stck : stack) (values : stackValue list) =
  List.fold_left push stck values

let get_n_values (stck : stack) (n : int) =
  if n <= 0 then
    []
  else
    match stck with
    | I32 v :: stck_tl -> I32 v :: get_n_values stck_tl (n - 1)
    | _ -> failwith "Not enough values on top of stack"

