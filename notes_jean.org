* Objectif du projet
Commencer à décrire une sémantique précise et réutilisable du Web Assembly.

Une sémantique en Ocaml du Web Assembly dépend de Ocaml qui n'est pas
sépcifié
Une sémantique en Isabelle du Web Assembly mais dépend de la sémantique
d'Isabelle qui est très compliqué car c'est une assistant de preuve.
Les assistant de preuve sont très compliqué car les preuve sont la
construction de terme qui correspond au type de la propriété. Ce concepte est
compliqué et n'est pas très proche des sémantique.

On aimerai avoir une sémantique dans un langage facilement manipulable.

* Plan
Il faut insister sur la partie 2. La partie 3 est légèrement intéressante.

* Intêret de la sémantique
Python est spécifié par l'implémentation, il n'y a pas du tout de sémantique.
C'est le même cas pour R.
Ocaml n'est pas specifié ! Donc il faut se baser sur l'exécution mais il
existe plusieurs façon de l'interpréter: (Interpreteur et Compilateur)

* Parler de la sémantqiue
La sémantique permet de raisonner plus facilement sur le fonctionnement
d'un programme.

C'est utile pour la vérification et la spécification.

Applications critique.

* Les modèles de sémantiques

Montrer les différentes manières de présenter une sémantique.

** Big step
Présenter ce que fait un programme par son résultat. Mais peu intéressant
pour des programmes qui ne termines pas ou qui intéragissent avec
l'exterieur. Ne permet pas de montrer les changements de l'environnement
Son avantage et qu'elle est très naturelle, c'est comme ça qu'on aimerai
le décrire. Big Step a plus de structure que Small Step car peut être
représenté comme une arbre.
** Small step
Enlèves ces problèmes mais ne correspond pas à ce qui se passe en vrai.
Small step travail sur les termes, il transforme les termes en d'autre. Mais
ce n'est pas le cas dans les programmes. Small Step a été décris avant Big
Step. Small step a peut de structure car c'est seulement une suite d'action.
* Sémantique catégorique
Il faudrait plutot parler de sémantique dénotationelle par opposition à la
sémantique opérationelle. On représente le programme par une fonction.
Certaine personnes se demande comment calculer la sémantique dénatotionelle
d'un programme. La sémantique dénotationelle ne s'interesse pas aux étapes
pour arriver aux résultat. C'est un domaine très théorique, mais ce n'est
pas forcément nécessaire d'en parler.

* Métalangages
Ils servent à être plus précis, d'unifier, d'introduire de la réusabilité.


* Misclenous
Un programme peut ne pas avoir de sémantique. Par exemple une implémentation
de la suite de syracuse. On pourrait dire que ce programme n'as pas de
sémantique implémentable car on ne sait pas si ça se termine.

* Operational Semantics

Lire Natural Semantics par Khan est une bonne idée, l'intro est très
intéressante et peut donner de bonnes idée.

* Technique de rédaction
Utiliser TODO notes

* Pretty-Big-Step Semantics

big-step and small-step are still usefull dependig on the aim.
The paper takes a long time justifying an improvement over big-step.

What does "in a store <something>" means ?

Pretty-big-step is based on big-step and aims to reduce the quantity of rules
inside the semantic definition as well as making it more robust.

In case of a semantic that can evaluate terms to error if the terms are not
well typed we would like to prove the soundness of this type system which
can be compromised if there is some rules missing. They introduce a generic
way to introduce rules on error so that none are missing. (An equivalence
with the original way of proving things on errors has been proven in Coq)
This generic way ease the specification because the rules that describe
errors can be created system.

* Web Assembly
Web assembly has an abstract syntax that is link to the binary format and
text format, we should write the rules on this binary syntax. There is a
finite number of memories and a stack, those memories should be represented
in the specification.

As base sort we could have literals such as floating number and integer
number. 

Contructors would be the instructions. We will think about modules later.

Program sort should only by instructions and control instruction.
Every instruction should change the state.

Would it be possible to juste have stack and linear memories as input state ?

* Web Assembly Specification

Je ne sais pas comment faire de petit-pas proprement en Web Assembly,
j'applique donc recursivement les règles sur le programme.

Il est nécessaire d'avoir accès à la suite du programme dans les règles car
la suite du programme peut être modifier par les règles.
(Instruction de controle tel que br)

CONST(t.const c; x) := [
    push(x_sigma, t, c) ?> x_sigma';
    H(x_sigma', x, x_o)
]

UNOP(t.unop; x) := [
    pop(x_sigma) ?> (x_sigma', c);
    isT(t, c) ?> c';
    unop(t, c') ?> r;
    push(x_sigma', t, r) ?> x_o
    H(x_sigma, x, x_o)
]

BINOP(t.binop) := [
    pop(x_sigma) ?> (x_sigma', c_2);
    isT(t, c_2) ?> c_2';
    pop(x_sigma) ?> (x_sigma', c_1);
    isT(t, c_1) ?> c_1';
    binop(t, c_1', c_2') ?> r;
    push(x_sigma', t, r) ?> x_o
]

BLOCK(block result_type; x) := [
    getNextInstructionInBlock(x) ?> x_i;
    pushLabel(x_sigma, 1, x_i) ?> x_sigma';
    H(x_sigma', x, x_o)
]

We will use filter for a lot of things here, because there is complicated
manipulation of the stack in form of loops which is complicated in skeletal
semantics

BR(br l; x) := [
    existsLabelWithDepth(x_sigma, l);
    getProgramAfterBlockWithDepth(l, x) ?> x_tl;
    getLabelArity(x_sigma, l) ?> x_a;
    isNValueOnTopOfStack(x_sigma, x_a);
    popN(x_sigma, x_a) ?> (x_sigma', x_vs);
    popUntilLabelWithDepth(x_sigma', l) ?> (x_sigma'', x_l);
    pushN(x_sigma'', x_vs) ?> x_sigma'''
    getLabelInstructionSequence(x_l) ?> x_i;
    appendInstructionSequence(x_i, x_tl) ?> x_n;
    H(x_sigma''', x_n, x_o)
]

Maybe we could suppose that a program has already gone through the program
created term that links instruction to the value pushed on the stack. for example
for the program. Cela devrait etre possible, car à tout point du programme
l'êtat du stack est connue

get_local $n
i32.const 1
i32.le
if
i32.const 1
else
get_local $n
i32.const 1
i32.add

==>

(if, (i32.le, get_local $n, i32.const 1), 

It might be interesting to have the skeletal semantic that can be interpreted
in a stack state that indicate what is on the stack. Such an interpretation
should be quite easy since the execution itself store in the stack the type
of each value. The difficulty, it it is one, will lie in the branches

Dans le travail de Guillaume et Maxime, il y a bien la notion de Big Step
semantic qui est retrouvé

Il serait bien de faire un programme Web Assembly dans necro avec ces règles.

Nous avons changer la façon dont nous approchons les règles pour les opérations
arithmétique. Précédement les règles était écrite en utilisant des
placeholders et il existait un filter pour chaque règle. Maintenant il y a
une règle principale sur un constructor qui prend le type de l'opérateur.
Cette règle appelle un hook sur le terme déterminant l'opération à faire.
Le problème est que ceci ne se factorise pas bien car les hooks ne peuvent
pas prendre deux arguments. On décidons donc de mettre un filtre prenant
en argument le terme déterminant l'opération à faire.

Nous choisissons de créer un constructeur pour les block du type:
nombre * instructions -> instructions
le nombre donne le nombre de résultat du block. La structure n'autorise 
qu'un seul résultat au maximum mais nous utilisons ce typage pour simplifier
la spécification et uniformiser avec les fonctions. De plus, la structure
de Web Assembly peut être représenter par un tel constructeur.

Un break ne dois pas vérifier qu'il ne pop pas une frame car c'est impossible
grâce à la validation.

* Présentation.

Après avoir présenté Skeletal Semantic, on dis que l'on vaut spécifié le
langage Web Assembly pour les raisons habituelles: langage utilisé pour tester
web assembly et les spécification habituelles sont pas ouf.

Expliquer les problèmes de small-step avec Skeletal Semantic à la fin de
l'explication de notre méthode.

** TODO Notes [1/20]
   - [ ] Discuter plus des différences entre les deux méthodes. En
     particulier dans la partie Big-Step il faut discuter des
     instructions de contrôle.

   - [X] Parler de necro qu'à la fin de la présentation.

   - [X] Peut être commencer par le méthode de small-step en
     premier. Et amener la méthode Big-Step qu'on peut avoir quelque
     chose de plus structurer.

   - [ ] "Et donc on a regardé comment avoir une description plus
     structuré". Il faudrait avoir un exemple de quelque chose de plus
     structuré. Faire le parallèle avec le if entre les deux
     méthodes. Pensé à mettre les deux à côté.

   - [X] Parler de Skeletal Semantic au début.

   - [X] Ajouter le nom des encadrants sur les slides.

   - [X] Introduire Skeletal Semantic au début au lieu de parler de
     Web Assembly en premier.

   - [ ] Dans les animations il faut faire attention qu'on n'est pas
     l'impression de revenir en arrière. Peut être mettre plus
     d'étape. On peut peut être dire qu'il y a des sauts structuré.

   - [ ] Corriger et simplifier la règle Br.

   - [ ] Discuter juste de comment faire la traduction est faite
     plutôt de ce que fait la règle "On transforme les boucles en
     filtres")

   - [X] Ne pas oublier d'indiquer que les branchements sont dans la
     skeletal   semantique.
     
   - [ ] Peut être montrer le texte de la spec rapidement pour
     s'appuyer dessus et montrer que ça fait des grosses règles car
     les règles sont compliqué.

   - [X] Utiliser des exemples pour décrire les grande idée ! Skeletal
     Semantic par exemple.

   - [ ] _Slide 10:_ être plus rapide

   - [X] _Slide 11:_ Il n'est pas nécessaire de laisser le nom des
     règles. Enlever le isNumber. Il ne sera pas nécessaire de mettre
     la règle du Unop car peut intéressante et il faudra mettre la
     règle du loop.

   - [ ] Indiquer la syntax du loop.

   - [ ] _Slide 12:_ Mettre ça à la fin et tenté de faire la démo.

   - [X] _Slide 14:_ Modifier le nom de l'approche, 
 
   - [X] _Slide 18:_ Expliquer qu'on doit trouver le matching end. Et
     dire que c'est non structuré. 

   - [ ] _Slide 19:_ Modifier pour enlever les truc qui est
     inintéressant. Dire qu'on a menti un peu après. Mettre la spec à
     côté.

   - [ ] _Slide 20:_ Less filters


* TODO
 - [] Demander l'accès au précédentes archives.
 - [] Décider de quelqu'un qui étudierai les introductions.
