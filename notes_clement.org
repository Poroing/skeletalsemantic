* Sortes
- termes
  - basiques
    + littéraux (lit)
    + identifiant (ident)
  - programmes
    + expr
      ++ entrée: state
      ++ sortie: val
    + statement (stat)
      ++ entrée: state
      ++ sortie: state
- flots
  + state
  + val
  + int
  + bool

* Constructeurs
$c : (s_{t_1}, ... s_{t_n}) -> s_t$

const : lit -> expr
var : ident -> expr
+ : (expr, expr) -> expr
skip : stat

* Filtres 
$f : (s_{t_1}, ... s_{t_n}) -> (s'_1, ... s'_m)$ 
Fonctions partielles donc cassent la branches si elles sont appliquées
sur un objet de mauvais type.

litToVal : lit -> val
read : ident * state -> val
add : int * int -> val
isInt : val -> int
isTrue : bool -> ()

* Précisions
** Différence entre interprétation abstraite et interprétation concrète
   Dans l'interprétation abstraite, on s'autorise des approximations. 
   Exemple: 
   - interprétation concrète -> réel
   - interprétation abstraite -> intervalles

** Plus petit point fixe 
   $\Cup_{n \in \mathbb{N} H^n(T)$
   $\Cap_{H(T) \subset T} T$

   Ne permet pas de capturer les boucles infinis -> revient a etre
   capable de prouver que le programmme termine

** Plus grand point fixe
   - $\Cup_{T \subset H(T)} T$
   - Plus de résultats mais ne permet pas les preuves de terminaisons.

   - Problème: Avec une boucle dans l'arbre d'inférence, on peut
     renvoyer n'importe quel résultat
   - Solution: utilisation de bottom, un résultat qui ne peut être
     construit autrement .

   - Bottom: il n'y a aucune execution qui conduit à cet état La
     concrétisation de bottom est l'ensemble vide
   - Top: La concrétisation de Top est l'ensemble de travail tout
     entier.

   - Si le programme termine, on est sur du résultat, si il ne termine
     pas, on revoit n'import quoi.

** Approximations
   On dit que \sigma est correct si en prenant une entrée plus grosse,
   la sortie contient les résultats pour la petite entrée.
   États vus comme des intervalles.

* Approche possibles
- Calculer une procédure de décision -> $T \subset H#(T)$
- +Squellette de PCF: Programming Computable Language.+
- +Squelette de ML.+
- Squelette de Web Assembly
  + https://webassembly.github.io/spec/core/index.html
  + https://github.com/WebAssembly/spec/blob/master/interpreter/exec/eval.ml
  + https://www.cl.cam.ac.uk/~caw77/papers/mechanising-and-verifying-the-webassembly-specification.pdf
  + http://seminaire-dga.gforge.inria.fr/2017/AndreasRossberg_en.html
  + https://people.mpi-sws.org/~rossberg/papers/Haas,%20Rossberg,%20Schuff,%20Titzer,%20Gohman,%20Wagner,%20Zakai,%20Bastien,%20Holman%20-%20Bringing%20the%20Web%20up%20to%20Speed%20with%20WebAssembly%20%5BDraft%5D.pdf
  + autre maniere de faire de la sémantique de maniere generique: K framework:  
    http://fsl.cs.illinois.edu/index.php/An_Overview_of_the_K_Semantic_Framework
  + lem et hot sewell

- Coder un autre générateur d'interpreteur Necro sans GTAD.
- question de relation entre parallélisme et sémantique. 

* Contact
alan.shchmitt@inria.fr
aile F, couleur orange
https://gitlab.inria.fr/skeletons/necro
http://homepages.inf.ed.ac.uk/gdp/publications/Math_Op_Sem.pdf

* Bibliographie
** rule format
** K semantics
*** Objectifs
    - Pouvoir supporter la concurrence, non déterminisme (parrallélisme?)
    - Modulaire
    - Pouvoir représenter n'importe quel concept actuellement utilisé
      en programmation
    - Non liée à un paradigme de programmation ou un langage
    - Executable
*** Principe
    - configurations, regles de réécfriture et calculs.
    - Le programme et les configurations du système sont représentés
      par des cellules (éventuellement emboitées les unes dans les
      autres).
    - Chaque cellule est etiquettée par un label.
    - Larges variété de types de cellules différentes: listes,
      ensembles, multiensemble etc...
    - Deux types de K-règles : computational and structural rules:
      + computationnal rule: correspond à une étape de calcul
      + structural rule: correspond à un réarrangement au sein d'une
        configuration.
    - un ensemble de K règles est un K système

**** K réécriture
**** K technique
     
** pretty big step: arthur chargueraud
** certified abstract interpretation with pretty big step semantics
** peter moses: derivating pretty big step from small step
* Rendu premier semestre
prendre un exemple générique: WHILE

* TODO [0/3]
  - [ ] Demander le rapport annoter a Steven Derrien.
  - [ ] Réécrire la semantique du Web assembly en Semantique squelettique.
  - [ ] L'implémenter en WebAssembly.
  - [ ] Lire le papier de Conrad Watts sur la spécification de Web Assembly en Isabelle. 
* Rédaction
  utiliser le package todonotes de LaTeX.
** Introduction
  - motiver le problème
  - dire ce que l'on va attendre
  - donner la structure du papier
** Pourquoi faire des sémantiques ?
   - c'est important qu'un langage soit defini pour les utilisateurs,
     les implémenteurs les gens qui font des outils de verif
   - avoir une spécification du language pour pouvoir les manipuler de
     manière rigoureuse.
   - faire de la vérification / compilation
   - il existes des langages spécifiés: C (avec plusieurs compilateurs
     (notamment compcert)), javascript)
   - c'est impossible de prouver qu'une analyse est correcte si l'on
     ne sait pas décrire comment une implémentation s'execute

** Différents modèles de sémantiques
   -> mettre en avant la grande variété de manières de penser la
      sémantique.
*** Sémantique dynamique
**** Big step
     Pratique d'associer un programme à son resultat. La strusture
     apparaît plus facilement et l'on voit clairement ce qui se passe.
     
     Problème, peut etre que celui ci ne termine pas ou qu'il a des
     effets de bords. On pourrait éventuellement retourner une trace d'éxecution

     Beaucoup de duplications. Difficile à mettre en
     oeuvre sur un ordi, exemple: pour le if, on ne sait pas quelle
     regle appliquer tant que l'on a pas executer le terme dans la
     condition.
     
     Plutot que dificile a implémenter: les cassures dans le flots de
     control (failwith etc...) duplique enormement le nombre de regles
     
     /!\ l'ordre d'évaluation n'est pas obligatoirement spécifié. 
**** Small step
     Plus facile de parler de programme non deterministe, qui ne
     termine pas ou qui interagissent avec l'extérieur.  

     On prend en entrée un état et un terme et on renvoit en sorti un
     état et un terme. dificulté: il fut capturer dans un terme, des
     termes partiellement exécuté: il faut reformer des termes à
     chaque fin de regle. Mais !! ca peut etre compliqué: par exemple
     la sémantique small step est artisanale et est similaire à un if,
     les structure ne sont pas naturelle et ne représente pas la
     réalité.

     Différence avec la réécriture, ici, on a de la
     mémoire en plus des termes.  

**** Pretty big step
**** Sémantique dénotationnelle
     Purement théorique: le sens du programme est donné par une
     fonction des entrées vers les sorties, sans autres explications.
     En big step, on donne un sens a un programme en donnant
     construisnat un arbre de dérivation de son exécution; en
     dénotationelle, on fait vraiment une fonction mathématique, plus précis et formel.

    Les problèmes de la sémantique dénotationelle sont a peu pres les meme que big step.

*** Sémantique pour la vérification
**** Sémantique abstraite/concrète

** Meta Language
   -> Avoir une sémantique pour les sémantiques pour que celles ci
      soient formelles, rigoureuses, réutilisables et les unifier.
   - motivation pour les meta langages, si pour un meme langage, il
     faut faire plusieurs sémantique, il est bon d'avoir un outil pour
     les formaliser, les relier / comparer etc..  -> ne pas réinventer
     la roue 
     -> être plus précis dans la manière de définir une
     sémantique 
     -> exemple compcert: initiallement en big step, ils
     ont tout recommencer pour obtenir une small step, alos que si ils
     avaient utilisé un meta langage, ca aurait été plus simple.

   - Quels sont les langages qui ont été modélisés dans les méta langages
   - Pour un méta langage donné qu'est ce que l'on peut faire avec
     (exporter en coq, LaTeX etc...)
*** OTT 
    - s'intéresse beaucoup à la notion de lieur
    - probème, OTT lui même n'a pas de sémantique .... 
*** LEM
*** K
    - K a une sémantique de réécriture, mais elle est compliqué...
    - c'est compliqué d'exprime les langages dont la sémantique ne se
      prete pas au formalisme de la réécriture utilisée en K.
    - compliqué si on veut faire autre chose que de la sémantique petit pas par réécriture
    - utilisé pour quels langages: web assembly par exemple
    - que peut on faire avec ?
    - 
*** SKELL
    - possiblilité d'utiliser des 
** Objectif
   - Il existe une spécification papier très précise du Webassembly
   - intérêt d'utiliser SKEL: voir si ca marche, avoir des outils réutilisables.
   - Faire une semantique simple et réutilisable de web assembly.
   - Celles ci existent deja:
     + en Caml mais du coup cela repose sur la sémantique de Caml qui
       n'est pas clairement définie.
     + en Isabelle HOL qui est super compliquée.
* Modifs
  - program that interact with the environment
  - revoir les aspects de concurrence
* Web Assembly
** Text format
   https://developer.mozilla.org/en-US/docs/WebAssembly/Understanding_the_text_format?fbclid=IwAR2vSP2r5C7WKQGLWwUDjQx7ORy5REahcLpOipfOuOcnN8Lk1e0s-E75eBU
*** Généralités
    - Un module webassembly est une suite de fonctions: /(module ...)/ 
    - Il y a 4 types différents: i32, i64, f32, f64.
    - Web assembly fonctionne sur le principe d'une machine a pile:
      pour effectuer une opération, on prends le bon nombre
      d'arguments sur la pile, on applique la fonction sur ces
      arguments et poussons le résultat sur la pile.
*** Organisation d'une fonction
    Chaque fonction s'organise de la manière suivante: 
    /(func $foo <signature> <locals> <body>)/
**** Signature
     - exemple: /(param $p1 i32) (param $p2 i64) (param $p3 f32) (result f64)/
     - Le nommage des paramètres est optionnel.
     - Si il n'y a pas de result, la fonction ne renvoit rien.
**** Variables locales
     - exemple: /(local $var1 i32)/
     - les variables utilisées dans le corps de la fonction doivent
       être déclarées de la sorte après la signature de la fonction.
**** Body
     Dans les exemples suivants, /t/ \in {i32, i64, f32, f64}.
     - Opérations arithmétiques élémentaires: /t.op/ où /op/ \in {add,
       sub, mul, div, abs, neg, ceil, floor, nearest, trunc, min, max,
       sqrt, eq, ge, le, lt, gt}
     - Définir une constante à mettre sur la pile: /t.const x/
     - Mettre sur la pile les variables locales ou les paramètres:
       /get_local i/. La numérotation commece à 0 et n'est pas
       réinitialisée au début des variables locales.
       Variante si la variable a été nommée: /get_local $var/
     - Donner une valeur à une variable locale: /set_locale/
     - Appeler une fonction d'une même module: /call $fun/
*** Autres 
     - Exporter une fonction: 
       /(export "lenomquejeveuxluidonner" (func $mafonction))/
     - Déclarer des variables globales:
       /(global $g (import "js" "global") (mut i32))/
     - Mémoire ruban (à importer de JS):
       /(import "js" "mem" (memory 1))/
       /(data (i32.const 0) "Hi")/
     - Tableau (pour stocker des refeerences de fonction, si on veut
       appeller une fonction dont le nom n'est connu qu'au runtime).
       Pour rappel, il n'y a pas de type fonction, ce qui empeche de
       mettre le nom de la fonction sur la pile.  
       + Déclaration d'un tableau et de taille /i/: /(table i anyfunc)/
       + Lister les fonctions référencées par la table (celles si ne
         sont pas forcément déja déclarées, on référence juste leur
         nom): /(elem (i32.const i) $f1 $f2 ...)/ le (i32.const i) est
         un offset.
       + Déclarer un type de retour /(type $return_i32 (func (result i32)))/
       + Appeler une fonction de la table: 
         /call_indirect (type $return_i32) (i32.const i)/ 
	 appelle la ieme fonction de la table. Il faut tout de même
         préciser le type de retour de la fonction et que celui ci
         soit en accord avec la focntion appelée.
       + exemple:
       /(module
         (table 2 anyfunc)
         (func $f1 (result i32)
           i32.const 42)
         (func $f2 (result i32)
           i32.const 13)
         (elem (i32.const 0) $f1 $f2)
         (type $return_i32 (func (result i32)))
         (func (export "callByIndex") (param $i i32) (result i32)
           get_local $i
           call_indirect (type $return_i32))
        )/ 
*** Control flow instructions
    - /block ... end/
    - /if t
       ...
       else
       ...
       end/ 
      où /t/ \in {i32, i64, f32, f64}. Possiblilité de sauter le else.
    - /loop $label/ définit un label
    - /br $label/
*** Exemples
**** Factorielle
    (module
      (func $fact (param $n i32) (return i32)
        get_local $n
	i32.const 0
	i32.eq
	if i32
	  i32.const 0
	else
	  get_local $n
	  i32.const 1
	  i32.sub 
	  call $fact
	  get_local $n
	  i32.mul
	end))
**** Fibonacci
    (module
      (func $fib (param $n i32) (return i32)
        get_local $n
	i32.const 0
	i32.eq
	if i32
	  i32.const 1
	else
	  get_local $n
	  i32.const 1
	  i32.eq
	  if i32
	    i32.const 1
	  else
	    get_local $n
	    i32.const 1
	    i32.sub 
	    call $fib
	    get_local $n
	    i32.const 2
	    i32.sub 
	    call $fib
	    i32.add
	  end
	end))
*** 

	  
