type value = int
type typWASM =
  I32 | I62 | F32 | F64
type index = int
           
type iunop =
  Clz | Ctz | Popcnt | Eqz
type ibinop =
  Add | Mul | Eq | Lt | Gt | Le | Ge
type numericInstr =
  Const of value | IUnop of iunop | IBinop of ibinop 
                                            
type controlInstr=
  Nop
| Unreachable
| Block of typWASM
| Loop of typWASM
| If of typWASM
| Else
| Br of index
| Br_if of index
| End 


type instr =
  NumericInstr of numericInstr | ControlInstr of controlInstr
                                               
type program = instr list
